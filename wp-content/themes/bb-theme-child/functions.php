<?php

// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );

remove_action('wp_head', 'wp_generator');
// Classes
require_once 'classes/class-fl-child-theme.php';

// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );

add_action( 'wp_enqueue_scripts', function(){
    wp_enqueue_script("slick",get_stylesheet_directory_uri()."/resources/slick/slick.min.js","","",1);
    wp_enqueue_script("cookie",get_stylesheet_directory_uri()."/resources/jquery.cookie.js","","",1);
     wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js","","",1);
});


// Register menus
function register_my_menus() {
    register_nav_menus(
        array(
            'footer-1' => __( 'Footer Menu 1' ),
            'footer-2' => __( 'Footer Menu 2' ),
            'footer-3' => __( 'Footer Menu 3' ),
            'footer-4' => __( 'Footer Menu 4' ),
            'footer-5' => __( 'Footer Menu 5' ),
            'site-map' => __( 'Site Map' ),
        )
    );
}
add_action( 'init', 'register_my_menus' );

// Enable shortcodes in text widgets
add_filter('widget_text','do_shortcode');

// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}

add_filter('wpseo_metabox_prio', 'yoasttobottom');

function fr_img($id=0,$size="",$url=false,$attr=""){

    //Show a theme image
    if(!is_numeric($id) && is_string($id)){
        $img=get_stylesheet_directory_uri()."/images/".$id;
        if(file_exists(to_path($img))){
            if($url){
                return $img;
            }
            return '<img src="'.$img.'" '.($attr?build_attr($attr):"").'>';
        }
    }

    //If ID is empty get the current post attachment id
    if(!$id){
        $id=get_post_thumbnail_id();
    }

    //If Id is object it means that is a post object, thus retrive the post ID
    if(is_object($id)){
        if(!empty($id->ID)){
            $id=$id->ID;
        }
    }

    //If ID is not an attachment than get the attachment from that post
    if(get_post_type($id)!="attachment"){
        $id=get_post_thumbnail_id($id);
    }

    if($id){
        $image_url=wp_get_attachment_image_url($id,$size);
        if(!$url){
            //If image is a SVG embed the contents so we can change the color dinamically
            if(substr($image_url,-4,4)==".svg"){
                $image_url=str_replace(get_bloginfo("url"),ABSPATH."/",$image_url);
                $data=file_get_contents($image_url);
                echo strstr($data,"<svg ");
            }else{
                return wp_get_attachment_image($id,$size,0,$attr);
            }
        }else if($url){
            return $image_url;
        }
    }
}

//Facet Title Hook
add_filter( 'facetwp_shortcode_html', function( $output, $atts ) {
    if ( isset( $atts['facet'] ) ) {       
        $output= '<div class="facet-wrap"><strong>'.$atts['title'].'</strong>'. $output .'</div>';
    }
    return $output;
}, 10, 2 );

//add method to register event to WordPress init

add_action( 'init', 'register_daily_mysql_bin_log_event');
 
function register_daily_mysql_bin_log_event() {
    // make sure this event is not scheduled
    if( !wp_next_scheduled( 'mysql_bin_log_job' ) ) {
        // schedule an event
        wp_schedule_event( time(), 'daily', 'mysql_bin_log_job' );
    }
}

add_action( 'mysql_bin_log_job', 'mysql_bin_log_job_function' );
 

function mysql_bin_log_job_function() {
   
    global $wpdb;
    $yesterday = date('Y-m-d',strtotime("-1 days"));
    $sql_delete = "PURGE BINARY LOGS BEFORE '$yesterday'" ;						
	$delete_endpoint = $wpdb->get_results($sql_delete);
    write_log($sql_delete);	
}

add_filter( 'auto_update_plugin', '__return_false' );


function mytheme_add_woocommerce_support() {
    add_theme_support( 'woocommerce' );
}

add_action( 'after_setup_theme', 'mytheme_add_woocommerce_support' );

//single product sheet functinality
function product_sheets_function(){

    global $product;
    $product = wc_get_product();
    $id = $product->ID;
    
    $content = '<ul>';

    if(!empty(get_field( 'tds', $id)) && get_field( 'tds', $id)!= 'None'){

        $content .= '<li><a href="'.get_field( 'tds', $id).'" target="_blank">TDS</a></li>';
    }
    if(!empty(get_field( 'sds', $id)) && get_field( 'sds', $id)!= 'None'){

        $content .= '<li><a href="'.get_field( 'sds', $id).'" target="_blank">SDS</a></li>';
    }
    if(!empty(get_field( 'sales_sheet', $id)) && get_field( 'sales_sheet', $id)!= 'None'){

        $content .= '<li><a href="'.get_field( 'sales_sheet', $id).'" target="_blank">SALE SHEET</a></li>';
    }
    if(!empty(get_field( 'systems_sheet', $id)) && get_field( 'systems_sheet', $id)!= 'None'){

        $content .= '<li><a href="'.get_field( 'systems_sheet', $id).'" target="_blank">SYSTEM SHEET</a></li>';
    }

    $content .= '</ul>';
    return  $content;
}
add_shortcode( 'product_sheets', 'product_sheets_function' );


function my_simple_custom_product_tab( $tabs ) {

	$tabs['my_custom_tab'] = array(
		'title'    => __( 'Additional Info', 'textdomain' ),
		'callback' => 'my_simple_custom_tab_content',
		'priority' => 90,
	);

	return $tabs;

}
add_filter( 'woocommerce_product_tabs', 'my_simple_custom_product_tab' );

function my_simple_custom_tab_content(){

    $meta_values = get_post_meta( get_the_ID() );
  
    $con .='<table class="table"><tbody>';

    if(array_key_exists("spec_0",$meta_values) && $meta_values['spec_0'][0]!=''){
     $con .= '<tr>           
            <td>'.$meta_values['spec_0'][0].'</td>
        </tr>';
     } 

    if(array_key_exists("spec_1",$meta_values) && $meta_values['spec_1'][0]!=''){ 
        $con .= '<tr>           
            <td>'.$meta_values['spec_1'][0].'</td>
        </tr>';
    } 

    if(array_key_exists("spec_2",$meta_values) && ($meta_values['spec_2'][0]!='' && $meta_values['spec_2'][0]!='None' )){ 
        $con .= '<tr>           
            <td>'.$meta_values['spec_2'][0].'</td>
        </tr>';
    } 

    if(array_key_exists("spec_3",$meta_values) && $meta_values['spec_3'][0]!=''){  
        $con .= '<tr>           
            <td>'.$meta_values['spec_3'][0].'</td>
        </tr>';
    } 
    if(array_key_exists("spec_4",$meta_values) && $meta_values['spec_4'][0]!=''){  
        $con .= '<tr>
              <td>'.$meta_values['spec_4'][0].'</td>
        </tr>';
    } 

    if(array_key_exists("spec_5",$meta_values) && $meta_values['spec_5'][0]!='') { 
        $con .= '<tr>
         <td>'.$meta_values['spec_5'][0].'</td>
        </tr>';
    } 
    if(array_key_exists("spec_6",$meta_values) && ($meta_values['spec_6'][0]!='' && $meta_values['spec_6'][0]!='None' )){ 
        $con .= '<tr>            
            <td>'.$meta_values['spec_6'][0].'</td>
        </tr>';
    } 

    $con .='</tbody>
</table>
</div>';

echo $con;

}


//remove addional info tab
add_filter( 'woocommerce_product_tabs', 'bbloomer_remove_product_tabs', 9999 );
  
function bbloomer_remove_product_tabs( $tabs ) {
    unset( $tabs['additional_information'] ); 
    return $tabs;
}

add_filter( 'woocommerce_catalog_orderby', 'misha_change_sorting_options_order', 5 );

function misha_change_sorting_options_order( $options ){
	
	$options = array(
		
		'menu_order' => __( 'Default sorting', 'woocommerce' ), // you can change the order of this element too
		'price'      => __( 'Sort by price: low to high', 'woocommerce' ), // I need sorting by price to be the first
		'date'       => __( 'Sort by latest', 'woocommerce' ), // Let's make "Sort by latest" the second one
		// and leave everything else without changes
		'popularity' => __( 'Sort by popularity', 'woocommerce' ),
		'rating'     => 'Sort by average rating', // __() is not necessary
		'price-desc' => __( 'Sort by price: high to low', 'woocommerce' ),
		
	);
	
	return $options;
}

add_action( 'woocommerce_variable_add_to_cart', 'bbloomer_update_price_with_variation_price' );
  
function bbloomer_update_price_with_variation_price() {
   global $product;
   $price = $product->get_price_html();
   wc_enqueue_js( "      
      $(document).on('found_variation', 'form.cart', function( event, variation ) {   
         if(variation.price_html) $('.summary > p.price').html(variation.price_html);
         $('.woocommerce-variation-price').hide();
      });
      $(document).on('hide_variation', 'form.cart', function( event, variation ) {   
         $('.summary > p.price').html('" . $price . "');
      });
   " );
}

/**
 * Function to return new placeholder image URL.
 */
function growdev_custom_woocommerce_placeholder( $image_url ) {
	$image_url = 'https://johnstonpaint.com/wp-content/uploads/2023/06/woocommerce-placeholder-1150x1150-1.png';  // change this to the URL to your custom placeholder
	return $image_url;
}
add_filter( 'woocommerce_placeholder_img_src', 'growdev_custom_woocommerce_placeholder', 10 );

add_action( 'init', 'custom_fix_thumbnail' );

    function custom_fix_thumbnail() {
    add_filter('woocommerce_placeholder_img_src', 'custom_woocommerce_placeholder_img_src');

    function custom_woocommerce_placeholder_img_src( $src ) {
    $upload_dir = wp_upload_dir();
    $uploads = untrailingslashit( $upload_dir['baseurl'] );
    $src = $uploads . '/2023/06/woocommerce-placeholder-1150x1150-1.png';

    return $src;
    }
}

add_filter( 'woocommerce_output_related_products_args', 'jk_related_products_args', 20 );
  function jk_related_products_args( $args ) {
	$args['posts_per_page'] = 5; // 4 related products
	return $args;
}
/**
* Hide Draft Pages from the menu
*/
function filter_draft_pages_from_menu ($items, $args) {
    foreach ($items as $ix => $obj) {
     if (!is_user_logged_in () && 'draft' == get_post_status ($obj->object_id)) {
      unset ($items[$ix]);
     }
    }
    return $items;
}
add_filter ('wp_nav_menu_objects', 'filter_draft_pages_from_menu', 10, 2);